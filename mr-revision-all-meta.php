<?php

/**
 * Plugin Name: Revision All Metadata
 * Plugin URI: https://bitbucket.org/mresults/
 * Description: Uses libraries provided by wp-post-meta-revisions to revision all metadata in all content types
 * Version: 0.0.1
 * Author: Marketing Results
 * Author URI: https://www.marketingresults.com.au
 * License: GPLv3
 * Bitbucket Plugin URI: mresults/mr-revision-all-meta
 * Bitbucket Branch:    master
 */
 
function mr_revision_all_meta_add_meta_keys_to_revision( $keys ) {
    
    global $wpdb;
    
    $results = $wpdb->get_results("SELECT meta_key FROM {$wpdb->postmeta} GROUP BY meta_key");
    
    foreach ($results as $result) {
        $keys[] = $result->meta_key;
    }
    return $keys;
}
add_filter( 'wp_post_revision_meta_keys', 'mr_revision_all_meta_add_meta_keys_to_revision' );
 
 ?>